let Parser = require('rss-parser');
const mongoose = require('mongoose');
const interval = require('interval-promise')
// Import the discord.js module
const Discord = require('discord.js');
// Create an instance of a Discord client
const client = new Discord.Client();
/*client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});*/
client.on('error', console.error);

//Mongoose config
mongoose.connect('mongodb://localhost/nodejs-rss-feeds', {
    useNewUrlParser: true
})
.then(() => { console.log('Mongodb Connected') })
.catch(() => { console.log('Something with MongodDB went wrong') });


//load model for Feeds
require('./models/News');
const News = mongoose.model('news');

let parser = new Parser();

interval(async () => {
    
    const rising_stack = await parser.parseURL('https://news.risingstack.com/rss').catch((err) => { console.log('Error setting risingstack parser' + err) });
    const node_source = await parser.parseURL('https://nodesource.com/blog/rss').catch((err) => { console.log('Error setting nodesource parser' + err) });
    const info_world = await parser.parseURL('https://www.infoworld.com/category/node-js/index.rss').catch((err) => { console.log('Error setting infoworld parser' + err) });
    const the_new_stack = await parser.parseURL('https://thenewstack.io/tag/node-js/rss').catch((err) => { console.log('Error setting thenewstack parser' + err) });
    const to_the_new = await parser.parseURL('http://www.tothenew.com/blog/category/technology/node-js-2/rss').catch(() => { console.log('Error setting tothenew parser' + err) });
    const nodeup = await parser.parseURL('https://feeds.feedburner.com/NodeUp?format=xml').catch((err) => { console.log('Error setting nodeup parser' + err) });
    const nodexperts = await parser.parseURL('https://nodexperts.com/blog/feed/').catch((err) => { console.log('Error setting nodexperts parser' + err) });
    const cronj = await parser.parseURL('https://www.cronj.com/blog/category/nodejs-web-development/feed/').catch((err) => { console.log('Error setting cronj parser' + err) });
    
    //News
    const nodejs_rss_feed_subscriptions = [rising_stack,node_source,info_world,the_new_stack,to_the_new,nodeup,nodexperts,cronj]
    try {
        nodejs_rss_feed_subscriptions.forEach((rss_feed) => {
            rss_feed.items.forEach(item => {
                if (item) {
                    News.findOne({ feed: item.link }).then(news => {
                        if (!news) {
                            let news = {};
                            news.feed = item.link
                            News.create(news)
                                .then(status => {
                                    client.channels.get('CHANNEL_ID').send(`[${item.title}] ${item.link}`);
                                })
                                .catch(err => {
                                    console.log(`News saving to db error: ${err}`)
                                })
                        } else {
                            //console.log('same feed')
                        }
                    })
                } else {
                    console.log('something wrong with: ' + rss_feed);
                }
            })
        });
    } catch (error) {
        console.log('news error: ' + error);
    }


    //Tutorials
    const code_for_geek = await parser.parseURL('https://codeforgeek.com/category/nodejs/feed/').catch((err) => { console.log('Error setting codeforgeek parser' + err) });
    const davidwalsh = await parser.parseURL('https://davidwalsh.name/tutorials/nodejs/feed').catch((err) => { console.log('Error setting davidwalsh parser' + err) });
    const airbrake = await parser.parseURL('https://airbrake.io/blog/category/nodejs/feed').catch((err) => { console.log('Error setting airbrake parser' + err) });
    const codebarbarian = await parser.parseURL('http://thecodebarbarian.com/feed.xml').catch((err) => { console.log('Error setting codebarbarian parser' + err) });
    const oktadeveloper = await parser.parseURL('https://feeds.feedburner.com/OktaDeveloperBlog').catch((err) => { console.log('Error setting oktadeveloper parser' + err) });

    const nodejs_rss_tutorials_subscriptions = [code_for_geek,davidwalsh,airbrake,codebarbarian,oktadeveloper]
    try {
        nodejs_rss_tutorials_subscriptions.forEach((rss_feed) => {
            rss_feed.items.forEach(item => {
                if (item) {
                    News.findOne({ feed: item.link }).then(news => {
                        if (!news) {
                            let news = {};
                            news.feed = item.link
                            News.create(news)
                                .then(status => {
                                    client.channels.get('CHANNEL_ID').send(`[${item.title}] ${item.link}`);
                                })
                                .catch(err => {
                                    console.log(`Tutorials saving to db error: ${err}`)
                                })
                        } else {
                            //console.log('same feed')
                        }
                    })
                } else {
                    console.log('something wrong with: ' + rss_feed);
                }
            })
        });
    } catch (error) {
        console.log('Tutorials error: ' + error);
    }

}, 900000);

client.login('DISCORD_BOT_TOKENID');