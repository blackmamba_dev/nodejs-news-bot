const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Scheme
const NodejsnewsScheme = new Schema({
    feed: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('news', NodejsnewsScheme);